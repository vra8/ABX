#!/usr/bin/env bash

if [ $# -lt 1 ]; then
    echo "Illegal number of parameters"
    echo
    echo "Usage: $0 <abx script name> [inputs]"
    echo "  <abx script name> - is the name of the javascript file with ABX function"
    echo "  [inputs] - inputs for the script in json format"
    echo
    exit 0
fi

if [ "$2" != '' ]; then
  node -e "console.log('Inputs:\n',JSON.stringify($2,null,2),'\n\n');"
fi

node -e "console.log('\n\nOutputs:\n',JSON.stringify(require('./$1').handler('',$2),null,2));"
