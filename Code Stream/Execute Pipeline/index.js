const fetch = require("node-fetch");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

exports.handler = function handler(context, inputs) {
    let settings = { method: 'POST', headers: { 'Content-Type': 'application/json'} };
    
    getToken = async () => {
        settings.body = JSON.stringify({'username': inputs.username, 'password': inputs.password});
        let response = await fetch(inputs.host + '/csp/gateway/am/api/login?access_token', settings);
        if (response.status == 200) {
            let json = await response.json();
            return json.access_token;
        } else {
            return "Error: " + JSON.stringify(response);
        }
    }
    
    executePipeline = async (token) => {
        settings.headers.Authorization = 'Bearer ' + token;
        settings.body = '{"input":{"dName":"' + inputs.deploymentName + '"}}';

        let response = await fetch(host + '/codestream/api/pipelines/' + inputs.pipelineId + '/executions', settings);
        if (response.status < 400) {
            let json = await response.json();
            return json.executionId;
        } else {
            return "Error: " + JSON.stringify(response) + response.status;
        }
    }

    let outputs = {};
    
    getToken()
    .then(token => {
        executePipeline(token)
        .then(data => {
            console.log("Execution ID: " + data);
            outputs.result = data;
        })
    });

    return outputs;
};
