const NSX = require('./nsx');

exports.handler = function(context, inputs) {
    const nsx = new NSX(inputs.host, inputs.user, inputs.password);

    const segment = {
        type: 'ROUTED',
        subnets: [{gateway_address: inputs.gateway, network: inputs.network}],
        connectivity_path: `/infra/tier-1s/${inputs.tier1}`,
        transport_zone_path: `/infra/sites/default/enforcement-points/default/transport-zones/${inputs.tz_id}`,
        display_name: `${inputs.segmentName}`
    };
    nsx.createSegment(inputs.segmentId, segment).then(res => {console.log(res)});
    return { foo: 'bar!' };
};
