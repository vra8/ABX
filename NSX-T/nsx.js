'use strict';
const fetch = require('node-fetch');

process.env.NODE_TLS_REJECT_UNAUTHORIZED=0;
process.env.NODE_NO_WARNINGS=1;

module.exports = class NSX {
    get API_ENDPOINT() { return '/policy/api/v1/infra' };

    constructor(host, user, password) {
        this.nsx_host = host;
        this.nsx_user = user;
        this.nsx_pwd  = password;
    }

    async REST(uri, method = 'GET', data = null) {
        const authz = Buffer.from(`${this.nsx_user}:${this.nsx_pwd}`).toString('base64');

        let settings = { method: method, headers: { 'Content-Type': 'application/json', Authorization: `Remote ${authz}` } };
        if (data !== null) settings.body = JSON.stringify(data);

        const response = await fetch(`https://${this.nsx_host}${uri}`, settings);
        if (response.status === 200) return await response.json();
        else { console.error(await response.json()); return null }
    }

    async GET(uri) { return await this.REST(uri, 'GET') };
    async DELETE(uri) { return await this.REST(uri, 'DELETE') };
    async PUT(uri, data) { return await this.REST(uri, 'PUT', data) };
    async PATCH(uri, data) { return await this.REST(uri, 'PATCH', data) };

    async getSegments() { return this.GET(`${this.API_ENDPOINT}/segments`) };
    async getSegmentWithId(id) { return this.GET(`${this.API_ENDPOINT}/segments/${id}`) };
    async createSegment(id, segment){ return this.PATCH(`${this.API_ENDPOINT}/segments/${id}`, segment) };

    host() { return this.nsx_host; };
};
